<?php //-*- coding: utf-8 -*-

/**
 * 整數の正規化
 *
 * @param  int $n   値
 * @param  int $base    基底數
 * @param  int $min 最小値
 * @return int      min≦ 値 ≦$base
 */
function fn_nmI($n, $base = 9, $min = 1)
{
    $base = (int)$base;
    while ($n > $base) {
        $n -= $base;
    }
    while ($n < $min) {
        $n += $base;
    }
    return $n;
}

/**
 * 値を、1..9 までの値に變換する。
 */
function fn_nm9($n)
{
    return fn_nmI($n, 9);
}




//*************************************************
/**
 * keep 'float' type. intval() or (int) cast returns int.
 * -4.5 は -4 にしたいよね。floor(-4.5)=-5 になる。
 *
 * @param  float|int|string $x0 數値
 * @return float    小數點以下をカットした値
 */
function fn_cut_decimal($x0)
{
    $x = (float)$x0;
    if ($x < 0.0) {
        return ceil($x);
    } else {
        return floor($x);
    }
}

/**
 * 角度の正規化
 *
 * @param  float $angle 角度
 * @return float    角度 0≦θ＜360
 */
function fn_nm($angle)
{
    if ($angle < 0) {
        $angle1 = -$angle;
        $angle2 = fn_cut_decimal($angle1 / 360);
        $angle1 -= 360 * $angle2;
        $angle1 = 360 - $angle1;
    } else {
        $angle1 = fn_cut_decimal($angle / 360);
        $angle1 = $angle - 360 * $angle1;
    }
        return($angle1);
}
